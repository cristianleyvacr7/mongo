package org.utl.conexionmysqlnube.modelo;

import java.time.LocalDate;

public class Peliculas {
    private int idPelicula;
    private String nombrePelicula;
    private String nombrePersona;
    private String fecha;
    private String genero;
    private String personajeFavorito;
    private String descripcion;

    public int getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(int idPelicula) {
        this.idPelicula = idPelicula;
    }

    public Peliculas() {
        this.idPelicula = idPelicula;
        this.nombrePelicula = nombrePelicula;
        this.nombrePersona = nombrePersona;
        this.fecha = fecha;
        this.genero = genero;
        this.personajeFavorito = personajeFavorito;
        this.descripcion = descripcion;
    }

    public Peliculas(int idPelicula, String nombrePelicula, String nombrePersona, String fecha, String genero, String personajeFavorito, String descripcion) {
        this.idPelicula = idPelicula;
        this.nombrePelicula = nombrePelicula;
        this.nombrePersona = nombrePersona;
        this.fecha = fecha;
        this.genero = genero;
        this.personajeFavorito = personajeFavorito;
        this.descripcion = descripcion;
    }

    public String getNombrePelicula() {
        return nombrePelicula;
    }

    public void setNombrePelicula(String nombrePelicula) {
        this.nombrePelicula = nombrePelicula;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getPersonajeFavorito() {
        return personajeFavorito;
    }

    public void setPersonajeFavorito(String personajeFavorito) {
        this.personajeFavorito = personajeFavorito;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
