package org.utl.conexionmysqlnube;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.utl.conexionmysqlnube.controlador.ConexionMYSQL;
import org.utl.conexionmysqlnube.modelo.Peliculas;

import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControlladorPeliculas implements Initializable {
    @FXML
    private Button btnEditar;

    @FXML
    private Button btnEliminar;

    @FXML
    private Button btnGuardar;

    @FXML
    private TableView<Peliculas> tblContenido;

    @FXML
    private TableColumn<Peliculas, String> tblcDescripcion;

    @FXML
    private TableColumn<Peliculas, String> tblcFecha;

    @FXML
    private TableColumn<Peliculas, String> tblcGenero;

    @FXML
    private TableColumn<Peliculas, String> tblcNombrePelicula;

    @FXML
    private TableColumn<Peliculas, String> tblcNombrePersona;

    @FXML
    private TableColumn<Peliculas, String> tblcPersonaje;

    @FXML
    private TableColumn<Peliculas, String> tblcidPelicula;

    @FXML
    private TextArea txtDescripcion;

    @FXML
    private TextField txtFecha;

    @FXML
    private TextField txtGenero;

    @FXML
    private TextField txtIdPelicula;

    @FXML
    private TextField txtNombrePelicula;

    @FXML
    private TextField txtNombrePersona;

    @FXML
    private TextField txtPersonajeFavorito;

    int myIndex;
    int id;


    public void save(ActionEvent event) throws Exception {
        String Tnombre, Tpelicula, Tfecha, Tgenero, Tpersonaje, Tdescripcion;
        Tnombre = txtNombrePersona.getText().toString();
        Tpelicula = txtNombrePelicula.getText().toString();
        Tfecha = txtFecha.getText().toString();
        Tgenero = txtGenero.getText().toString();
        Tpersonaje = txtPersonajeFavorito.getText().toString();
        Tdescripcion = txtDescripcion.getText().toString();
        String sql = "INSERT INTO Peliculas(nombrePelicula,nombrePersona,fecha,genero,personajeFavorito, descripcion)VALUES(?,?,?,?,?,?)";

        ConexionMYSQL mySQLNube = new ConexionMYSQL();
        Connection conn = mySQLNube.open();
        CallableStatement statement = conn.prepareCall(sql);
        statement.setString(1, Tnombre);
        statement.setString(2, Tpelicula);
        statement.setString(3, Tfecha);
        statement.setString(4, Tgenero);
        statement.setString(5, Tpersonaje);
        statement.setString(6, Tdescripcion);

        statement.executeUpdate();
        statement.close();
        mySQLNube.close();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        read();
        clean();
        }
    public void read()throws Exception{
        ObservableList<Peliculas> pelisList = FXCollections.observableArrayList();
        try {
            String sql = "SELECT * FROM peliculas";
            ConexionMYSQL mySQLNube = new ConexionMYSQL();
            Connection conn = mySQLNube.open();
            CallableStatement statement = conn.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()){
                Peliculas p = new Peliculas();
                p.setIdPelicula(rs.getInt("idPelicula"));
                p.setNombrePersona(rs.getString("nombrePersona"));
                p.setNombrePelicula(rs.getString("nombrePelicula"));
                p.setFecha(rs.getString("fecha"));
                p.setGenero(rs.getString("genero"));
                p.setPersonajeFavorito(rs.getString("personajeFavorito"));
                p.setDescripcion(rs.getString("descripcion"));
                pelisList.add(p);
            }
            tblContenido.setItems(pelisList);
            tblcidPelicula.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getIdPelicula()).asString());
            tblcNombrePersona.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getNombrePersona()));
            tblcNombrePelicula.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getNombrePelicula()));
            tblcFecha.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getFecha()));
            tblcGenero.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getGenero()));
            tblcPersonaje.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getPersonajeFavorito()));
            tblcDescripcion.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getDescripcion()));
        }catch (SQLException ex){
            Logger.getLogger(ControlladorPeliculas.class.getName()).log(Level.SEVERE,null,ex);
        }
        tblContenido.setRowFactory(index ->{
            TableRow<Peliculas> peliculasTableRow = new TableRow<>();
            peliculasTableRow.setOnMouseClicked(event -> {
                if(event.getClickCount()==1 &&(!peliculasTableRow.isEmpty())){
                    myIndex = tblContenido.getSelectionModel().getSelectedIndex();
                    id = Integer.parseInt(String.valueOf(tblContenido.getItems().get(myIndex).getIdPelicula()));
                    txtNombrePersona.setText(tblContenido.getItems().get(myIndex).getNombrePersona());
                    txtNombrePelicula.setText(tblContenido.getItems().get(myIndex).getNombrePelicula());
                    tblcFecha.setText(tblContenido.getItems().get(myIndex).getFecha());
                    txtGenero.setText(tblContenido.getItems().get(myIndex).getGenero());
                    txtPersonajeFavorito.setText(tblContenido.getItems().get(myIndex).getPersonajeFavorito());
                    tblcDescripcion.setText(tblContenido.getItems().get(myIndex).getDescripcion());
                }
            });
            return peliculasTableRow;
        });
    }
    public void update(ActionEvent event) throws Exception {
        String Tnombre, Tpelicula, Tfecha, Tgenero, Tpersonaje, Tdescripcion;
        myIndex = tblContenido.getSelectionModel().getSelectedIndex();
        id = Integer.parseInt(String.valueOf(tblContenido.getItems().get(myIndex).getIdPelicula()));
        Tnombre = txtNombrePersona.getText().toString();
        Tpelicula = txtNombrePelicula.getText().toString();
        Tfecha = txtFecha.getText().toString();
        Tgenero = txtGenero.getText().toString();
        Tpersonaje = txtPersonajeFavorito.getText().toString();
        Tdescripcion = txtDescripcion.getText().toString();

        String sql = "UPDATE peliculas SET nombrePersona = ?,nombrePelicula = ?,fecha = ?,genero = ?,personajeFavorito =?, descripcion =? where idPelicula =?";

        ConexionMYSQL mySQLNube = new ConexionMYSQL();
        Connection conn = mySQLNube.open();
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, Tnombre);
        statement.setString(2, Tpelicula);
        statement.setString(3, Tfecha);
        statement.setString(4, Tgenero);
        statement.setString(5, Tpersonaje);
        statement.setString(6, Tdescripcion);
        statement.setInt(7,id);

        statement.executeUpdate();
        statement.close();
        mySQLNube.close();
        read();
        clean();


    }


    public void delete(ActionEvent event) throws Exception {
        String sql = "DELETE FROM peliculas where idPelicula = ?";
        myIndex = tblContenido.getSelectionModel().getSelectedIndex();
        id = Integer.parseInt(String.valueOf(tblContenido.getItems().get(myIndex).getIdPelicula()));
        try{
            ConexionMYSQL mySQLNube = new ConexionMYSQL();
            Connection conn = mySQLNube.open();
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
        }catch (SQLException ex){
            Logger.getLogger(ControlladorPeliculas.class.getName()).log(Level.SEVERE,null,ex);
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        read();

    }
    public void clean(){
        txtNombrePersona.setText("");
        txtNombrePelicula.setText("");
        txtFecha.setText("");
        txtGenero.setText("");
        txtPersonajeFavorito.setText("");
        txtDescripcion.setText("");

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            read();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
