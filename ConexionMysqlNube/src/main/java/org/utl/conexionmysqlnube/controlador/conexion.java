package org.utl.conexionmysqlnube.controlador;

import javafx.application.Application;
import javafx.stage.Stage;

public class conexion {

    public static void main(String[] args) {
        ConexionMYSQL connMySQL = new ConexionMYSQL();

        try{
            connMySQL.open();
            System.out.println("Conexion establecida con MySQL");

            connMySQL.close();
            System.out.println("Se ha cerrado la conexion con MySQL");
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
