module org.utl.conexionmysqlnube {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens org.utl.conexionmysqlnube to javafx.fxml;
    exports org.utl.conexionmysqlnube;
}